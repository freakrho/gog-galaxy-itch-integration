import asyncio
import json
import re
import logging
import pathlib
from urllib import parse
from urllib.parse import parse_qs
from typing import Dict, List, Union, Any, Optional
from dataclasses import dataclass

from constants import *
from butler.butler import Butler
from butler.methods import Methods
from galaxy.api.errors import AccessDenied, InvalidCredentials
from galaxy.api.plugin import Plugin
from galaxy.api.consts import Platform, OSCompatibility, LicenseType, LocalGameState
from galaxy.api.types import NextStep, Authentication, GameLibrarySettings, GameTime, FriendInfo, LocalGame, \
    Achievement, Game, SubscriptionGame, Subscription, UserPresence, UserInfo, LicenseInfo

logger = logging.getLogger(__name__)
file_dir = os.path.dirname(os.path.abspath(__file__))

def regex_pattern(regex):
    return ".*" + re.escape(regex) + ".*"

def captcha_params(url: str) -> dict :
    return {
        "window_title": "Fill captcha",
        "window_width": 536,
        "window_height": 675,
        "start_uri": url,
        "end_uri_regex": regex_pattern("https://www.google.com/recaptcha/api2/userverify?k="),
    }

class Itch(Plugin):
    @dataclass
    class InstalledGame:
        game_id: str
        cave_id: str
        install_folder: str

    @dataclass
    class InstallationEvent:
        id: str
        game_id: str
        cave_id: str
        install_folder: str

    def __init__(self, reader, writer, token):
        super().__init__(
            Platform.ItchIo,
            VERSION,
            reader,
            writer,
            token
        )
        self._install_id = 0
        self._users = []  # type: List[dict]
        self._butler = Butler()
        self._butler.subscribe_method(Methods.Profile.RequestCaptcha, self._request_captcha)
        self._game_cache = {}
        self._local_games: Dict[str, Itch.InstalledGame] = {}
        self._installation_events = {}
        self._default_install_path = ""
        self._captcha_url = ""
        self._captcha_id = 0
        self._login_response = None
        if os.path.exists(ITCH_PREFERENCES_PATH):
            preferences_file = open(ITCH_PREFERENCES_PATH, "r")
            itch_preferences = json.load(preferences_file)
            if "installLocations" in itch_preferences:
                install_locations = itch_preferences["installLocations"]
                default_install_location_id = itch_preferences["defaultInstallLocation"]
                if default_install_location_id in install_locations.keys():
                    self._default_install_path = install_locations[default_install_location_id]["path"]

    # STATIC METHODS
    @staticmethod
    def is_stale(data: dict):
        return "stale" in data and data["stale"]

    def _user_added(self, profile_id: int):
        for profile in self._users:
            if profile["id"] == profile_id:
                return True
        return False

    @staticmethod
    def get_login_params() -> Dict[str, str]:
        url = pathlib.Path(file_dir).as_uri()
        logger.debug("URL: " + url)
        return {
            "window_title": "Log in to Itch.io",
            "window_width": 536,
            "window_height": 675,
            "start_uri": url + "/" + SERVER + "/index.html",
            "end_uri_regex": regex_pattern(url + "/" + SERVER + "/" + URI),
        }

    @staticmethod
    def get_license_type(game_data: dict):
        if "canBeBought" in game_data and game_data["canBeBought"]:
            return LicenseType.SinglePurchase
        return LicenseType.FreeToPlay

    # BUTLER RPC
    async def _rpc_butler(self, method: str, params: dict):
        got_data = False
        data = {}

        def got_answer(result: dict):
            nonlocal got_data
            nonlocal data
            got_data = True
            data = result
            logger.info("RPC response: " + str(data))
        self._butler.send_rpc(method, params, got_answer)

        async def wait_for_data():
            nonlocal got_data
            nonlocal data
            time = 0
            while not got_data:
                if time > MAX_WAIT_TIME:
                    got_data = True
                    data = {
                        ERROR: {
                            "message": "timeout",
                            "code": 0
                        }
                    }
                time += SLEEP
                await asyncio.sleep(SLEEP)

        await self.create_task(wait_for_data(), "Wait for butler response")

        if ERROR in data:
            error = data[ERROR]
            error_code = error["code"]
            if error_code == 12000:  # API error
                status_code = error["data"]["apiError"]["statusCode"]
                if status_code == 403:  # scope error
                    # do login again
                    await self._flush_users()
        return data

    def _rpc_butler_async(self, method: str, params: dict, callback):
        self._butler.send_rpc(method, params, callback)

    def _request_captcha(self, data: dict):
        self._captcha_id = data["id"]
        self._captcha_url = data["params"]["recaptchaUrl"]

    # INTERNAL METHODS
    async def _flush_users(self):
        self.store_credentials({})
        logger.info("Force GOG Galaxy to do login again")
        for user in self._users:
            await self._rpc_butler(Methods.Profile.Forget, {"profileId": user[ID]})
        self.lost_authentication()

    def _do_login_with_key_async(self, access_token: str):
        self._rpc_butler_async(Methods.Profile.LoginWithAPIKey, {"apiKey": access_token}, self._get_login_response)

    async def _do_login_with_key(self, access_token: str):
        data = await self._rpc_butler(Methods.Profile.LoginWithAPIKey, {"apiKey": access_token})
        return self._do_login(data)

    def _do_login_with_password(self, username: str, password: str):
        self._rpc_butler_async(Methods.Profile.LoginWithPassword, {
            "username": username,
            "password": password
        }, None)

    async def _get_user_owned_games(self, user: dict, game_list: list):
        # Get keys
        keys = await self._rpc_butler(Methods.Fetch.ProfileOwnedKeys, {
            "profileId": user["id"]
        })

        if ERROR not in keys:
            if Itch.is_stale(keys):
                new_keys = await self._rpc_butler(Methods.Fetch.ProfileOwnedKeys, {
                    "profileId": user["id"],
                    "fresh": True
                })
                if ERROR not in new_keys:
                    keys = new_keys

            for key in keys["items"]:
                game_item = key["game"]
                self._game_cache[str(game_item["id"])] = game_item
                game = Game(
                    game_id=game_item["id"],
                    game_title=game_item["title"],
                    license_info=LicenseInfo(Itch.get_license_type(game_item)),
                    dlcs=None
                )
                game_list.append(game)

        # Get local games
        data = await self._rpc_butler(Methods.Fetch.Caves, {})
        if ERROR not in data:
            caves = data["items"]
            if caves is not None:
                for cave in caves:
                    game_item = cave["game"]
                    self._game_cache[str(game_item["id"])] = game_item
                    game = Game(
                        game_id=game_item["id"],
                        game_title=game_item["title"],
                        license_info=LicenseInfo(Itch.get_license_type(game_item)),
                        dlcs=None
                    )
                    game_list.append(game)

    def _get_installation_event(self, cave_id: str) -> InstallationEvent:
        for event in self._installation_events:
            if event.cave_id == cave_id:
                return event

    def _on_install_game(self, data: dict):
        if ERROR not in data:
            event = self._get_installation_event(str(data["caveId"]))
            if event:
                if event.game_id in self._local_games:
                    local_game = self._local_games[event.game_id]
                    local_game.cave_id = event.cave_id
                    local_game.install_folder = event.install_folder
                else:
                    local_game = self.InstalledGame(game_id=event.game_id,
                                                    cave_id=event.cave_id,
                                                    install_folder=event.install_folder)
                self._local_games[event.game_id] = local_game
                self.update_local_game_status(LocalGame(local_game.game_id, LocalGameState.Installed))

        logger.debug("Installed " + str(data))

    def _get_install_id(self) -> int:
        self._install_id += 1
        return self._install_id

    def _get_cached_game(self, game_id: str) -> Optional[Dict]:
        if game_id in self._game_cache:
            return self._game_cache[game_id]

    async def _fetch_game(self, game_id: str) -> Optional[Dict]:
        game = await self._rpc_butler(Methods.Fetch.Game, {"gameId": int(game_id)})

        if Itch.is_stale(game):
            result = await self._rpc_butler(Methods.Fetch.Game, {"gameId": int(game_id), "fresh": True})
            if ERROR not in result and "game" in result:
                self._game_cache[game_id] = result
                return result["game"]
            else:
                return self._get_cached_game(game_id)

        if ERROR in game or "game" not in game:
            return self._get_cached_game(game_id)

        self._game_cache[game_id] = game
        return game["game"]

    def _get_login_response(self, data: dict):
        self._login_response = data

    def _do_login(self, data: dict):
        if "profile" in data:
            user = data["profile"]["user"]
            if not self._user_added(user["id"]):
                self._users.append(user)
            return Authentication(user["id"], user["username"])

    async def _find_local_users(self) -> bool:
        profiles = await self._rpc_butler(Methods.Profile.List, {})
        if PROFILES in profiles:
            profiles = profiles[PROFILES]
            if profiles is not None:
                for profile in profiles:
                    self._users.append(profile[USER])
            logger.debug("Users: {users}".format(users=self._users))
            self.store_credentials({"profiles": profiles})
            if len(self._users) > 0:
                return True
        return False

    # PLUGIN METHODS
    async def authenticate(self, stored_credentials=None):
        if await self._find_local_users():
            return Authentication(self._users[0][ID], self._users[0][USERNAME])

        return NextStep("web_session", Itch.get_login_params())

    async def pass_login_credentials(self, step: str, credentials: Dict[str, str], cookies: List[Dict[str, str]]) -> \
            Union[NextStep, Authentication]:
        if self._find_local_users():
            return Authentication(self._users[0][ID], self._users[0][USERNAME])

        raise InvalidCredentials()

    async def get_owned_games(self) -> List[Game]:
        game_list = []
        for user in self._users:
            await self._get_user_owned_games(user, game_list)
        for game in game_list:
            logger.debug("Owned Game %s - %s " % (game.game_id, game.game_title))
        return game_list

    async def get_local_games(self) -> List[LocalGame]:
        data = await self._rpc_butler(Methods.Fetch.Caves, {})
        if ERROR in data:
            return []

        game_list = []
        caves = data["items"]
        if caves is not None:
            for cave in caves:
                game_item = cave["game"]
                self._game_cache[str(game_item["id"])] = game_item
                installed_game = self.InstalledGame(game_id=str(game_item["id"]), cave_id=cave["id"],
                                                    install_folder=cave["installInfo"]["installFolder"], )
                self._local_games[installed_game.game_id] = installed_game
                game = LocalGame(installed_game.game_id, LocalGameState.Installed)
                game_list.append(game)
        return game_list

    async def install_game(self, game_id: str) -> None:
        game = await self._fetch_game(game_id)

        # create install location
        path = os.path.join(self._default_install_path, game["title"]) + os.sep
        if not os.path.exists(path):
            os.mkdir(path)
        location = await self._rpc_butler(Methods.Install.Locations.Add, {
            "path": path
        })

        if ERROR in location:
            # TODO: Throw exception
            return

        queue = await self._rpc_butler(Methods.Install.Queue, {
            "game": game,
            "installLocationId": location["installLocation"]["id"]
        })
        logger.debug("Queued install: " + str(queue))
        if ERROR not in queue:
            event = self.InstallationEvent(id=str(self._get_install_id()), game_id=game_id,
                                           cave_id=queue["caveId"], install_folder=queue["installFolder"])
            self._installation_events[event.id] = event
            self._rpc_butler_async(Methods.Install.Perform, {
                "id": event.id,
                "stagingFolder": queue["stagingFolder"]
            }, self._on_install_game)

    async def uninstall_game(self, game_id: str) -> None:
        if game_id in self._local_games.keys():
            game = self._local_games[game_id]
            logger.info("Uninstalling game " + game.game_id)
            self._rpc_butler_async(Methods.Uninstall.Perform, {
                "caveId": game.cave_id
            }, None)

    async def launch_game(self, game_id: str) -> None:
        if game_id in self._local_games.keys():
            game = self._local_games[game_id]
            logger.info("Launching game " + game.game_id)
            self._rpc_butler_async(Methods.Launch, {
                "caveId": game.cave_id,
                "prereqsDir": os.path.join(game.install_folder, "prereqs")
            }, None)

    async def get_os_compatibility(self, game_id: str, context: Any) -> Optional[OSCompatibility]:
        logger.debug("Fetching game with ID " + game_id)
        game = await self._fetch_game(game_id)
        logger.debug("Got game " + game_id + ": " + str(game))
        if game is not None:
            compatible_os = OSCompatibility(0)
            logger.debug(game)
            platforms = game["platforms"]
            if "windows" in platforms:
                compatible_os |= OSCompatibility.Windows
            if "linux" in platforms:
                compatible_os |= OSCompatibility.Linux
            if "osx" in platforms:
                compatible_os |= OSCompatibility.MacOS
            return compatible_os

    async def get_local_size(self, game_id: str, context: Any) -> Optional[int]:
        if game_id in self._local_games.keys():
            size = await self._rpc_butler(Methods.System.StatFS, {
                "path": self._local_games[game_id].install_folder
            })
            if ERROR not in size and "totalSize" in size:
                return size["totalSize"]

    # async def get_game_library_settings(self, game_id: str, context: Any) -> GameLibrarySettings:
    #     pass

    # async def get_unlocked_achievements(self, game_id: str, context: Any) -> List[Achievement]:
    #     pass
    #
    # async def launch_platform_client(self) -> None:
    #     pass

    # async def shutdown_platform_client(self) -> None:
    #     pass
    #
    # async def get_friends(self) -> List[UserInfo]:
    #     pass
    #
    # async def get_game_time(self, game_id: str, context: Any) -> GameTime:
    #     pass
    #
    # async def get_user_presence(self, user_id: str, context: Any) -> UserPresence:
    #     pass
    #
    # async def get_subscriptions(self) -> List[Subscription]:
    #     pass
    #
    # async def get_subscription_games(self, subscription_name: str, context: Any) -> AsyncGenerator[
    #     List[SubscriptionGame], None]:
    #     pass
