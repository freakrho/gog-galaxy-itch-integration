import os
import sys

from galaxy.api.plugin import create_and_run_plugin
from plugin import Itch


def main():
    sys.stdout = open(os.path.join(os.path.dirname(__file__), "log.txt"), "w")
    print("start")
    create_and_run_plugin(Itch, sys.argv)


# run plugin event loop
if __name__ == "__main__":
    main()
