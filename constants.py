import os


VERSION = "0.1"
SERVER = "login_server"
URI = "galaxy_itch"
MAX_WAIT_TIME = 10  # seconds
SLEEP = 0.1  # seconds
ERROR = "error"
ITCH_PREFERENCES_FILE = "preferences.json"
if os.name == 'nt':
    ITCH_INSTALL_DIR = os.path.expandvars(r'%APPDATA%\itch')
else:
    ITCH_INSTALL_DIR = os.path.dirname(os.path.abspath(__file__))
ITCH_PREFERENCES_PATH = os.path.join(ITCH_INSTALL_DIR, ITCH_PREFERENCES_FILE)
ACCESS_TOKEN = "access_token"
USER = "user"
ID = "id"
USERNAME = "username"
PROFILES = "profiles"